
<head>
<meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<link href = "{{asset('public/css/main.css')}}" rel = "stylesheet">
<link href = "http://demos.codexworld.com/add-date-time-picker-input-field-jquery/jquery.datetimepicker.css"
         rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "http://demos.codexworld.com/add-date-time-picker-input-field-jquery/jquery.datetimepicker.full.js"></script>
<script src = "{{asset('public/js/main.js')}}"></script>
<div class="container">
  <form action="{{route('EditSettingRecord')}}" class="myform" method="post" id="myform" name="myform">
  @csrf
  @if(Session::has('message'))
  <span style="color:green;">{{ Session::get('message') }}</span>
  @endif
  <input type="hidden" name='id' value='{{$Record->id}}'>
    <div class="row">
      <div class="col-25">
        <label for="fname">N</label>
      </div>
      <div class="col-75">
        <input type="text" id="n" onfocus="HideError('n');" name="n"  value='{{$Record->n}}' placeholder="Fill Up.." required>
        <span id='n_error' style="color:red;display:none;"></span>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="fname">D</label>
      </div>
      <div class="col-75">
      <select name="d" id="d" onfocus="HideError('d');">
      <option value="">Select Value</option>
      <option value="day" @if($Record->d=='day'){{'selected'}}@endif>Day</option>
      <option value="week" @if($Record->d=='week'){{'selected'}}@endif>Week</option>
      <option value="month" @if($Record->d=='month'){{'selected'}}@endif>Month</option>
      </select>
        <span id='d_error' style="color:red;display:none;"></span>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="fname">G</label>
      </div>
      <div class="col-75">
      <select name="g" id="g" onfocus="HideError('g');">
      <option value="">Select Value</option>
      <option value="individual"  @if($Record->g=='individual'){{'selected'}}@endif>individual</option>
      <option value="group"  @if($Record->g=='group'){{'selected'}}@endif>group</option>
      </select>
        <span id='g_error' style="color:red;display:none;"></span>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="fname">TZ</label>
      </div>
      <div class="col-75">
      <select name="tz" id="tz" onfocus="HideError('tz');">
      <option value="">Select Value</option>
      <option value="utc"  @if($Record->tz=='utc'){{'selected'}}@endif>UTC</option>
      <option value="Asia/Kolkata"  @if($Record->tz=='Asia/Kolkata'){{'selected'}}@endif>Asia/Kolkata</option>
      <option value="America/NewYork"  @if($Record->tz=='America/NewYork'){{'selected'}}@endif>America/NewYork</option>
      </select>
        <span id='tz_error' style="color:red;display:none;"></span>
      </div>
    </div>
    <center>
    <div class="row">
      <input type="button" class="editform" value="Submit">
    </div>
    </center>
  </form>
</div>

