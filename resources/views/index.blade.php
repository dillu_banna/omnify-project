<head>
<meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<link href = "{{asset('public/css/main.css')}}" rel = "stylesheet">
<link href = "http://demos.codexworld.com/add-date-time-picker-input-field-jquery/jquery.datetimepicker.css"
         rel = "stylesheet">
      <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
      <script src = "http://demos.codexworld.com/add-date-time-picker-input-field-jquery/jquery.datetimepicker.full.js"></script>
      <script src = "{{asset('public/js/main.js')}}"></script>
<div class="container">
  <form  class="myform" id="myform" name="myform">
    <div class="row">
      <div class="col-25">
        <label for="fname">User IDs</label>
      </div>
      <div class="col-75">
        <input type="text" id="userid" onfocus="UserIDError();" name="userid" placeholder="Insert IDs.." required>
        <span id='userid_error' style="color:red;display:none;"></span>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="lname">Select Date</label>
      </div>
      <div class="col-75">
        <input type="text" id = "reservation_date" onfocus="DateError();" required name="reservation_date" placeholder="Select Your Date..">
        <span id='date_error' style="color:red;display:none;"></span>
      </div>
    </div>
    
   

    <center>
    <div class="row">
      <input type="button" class="submitform" value="Submit">
    </div>
    </center>
  </form>
</div>
<script>
//Datepicker
$('#reservation_date').datetimepicker({
    format:'d-m-Y H:i',
});
</script>