//Validation error message hide function start
function HideError(value)
{
    if(value=='n'){
    $('#n_error').hide();
    return false;
    }
    if(value=='d'){
    $('#d_error').hide();
    return false;
    }
    if(value=='g'){
    $('#g_error').hide();
    return false;
    }
    if(value=='tz'){
    $('#tz_error').hide();
    return false;
    }
}
//Validation error message hide function end



//submit from data function start
$(document).ready(function () {
$(".editform").click(function(){
  if($('#n').val()=="")
  {
    $('#n_error').show();
    $("#n_error").html("Please Fill Value");
    return false;
  }
  else if($('#d').val()=="")
  {
    $('#d_error').show();
    $("#d_error").html("Please Select Value");
    return false;
  }
  else if($('#g').val()=="")
  {
    $('#g_error').show();
    $("#g_error").html("Please Select Value");
    return false;
  }
  else if($('#tz').val()=="")
  {
    $('#tz_error').show();
    $("#tz_error").html("Please Select Value");
    return false;
  }
  else{
    $( "#myform" ).submit();
  }
   });
}); 
//submit from data function end


function UserIDError()
{
    $('#userid_error').hide();
    return false;
}
function DateError()
{
    $('#date_error').hide();
    return false;
}
//  $(function() {
//     $( "#reservation_date" ).datepicker();
    
// });


//Form submit by ajax
$(document).ready(function () {

$(".submitform").click(function(){
  if($('#userid').val()=="")
  {
    $('#userid_error').show();
    $("#userid_error").html("Please Insert UserIds");
    return false;
  }
  else if($('#reservation_date').val()=="")
  {
    $('#date_error').show();
    $("#date_error").html("Please Select Date");
    return false;
  }
  else{
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    $.ajax({
               type:'POST',
               url:'/Omnify-Test/ValidateFormData',
               data: {
                     _token: CSRF_TOKEN,
                     UserIds: jQuery('#userid').val(),
                     Reservationdate: jQuery('#reservation_date').val()
                  },
               success:function(data) {
                 if(data.status==0){
                  alert(data.message);
                 }
                 else{
                  alert(data.message);
                  location.reload(); 
                 }
               }
            });
  }
});

});    