<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ValidateController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::any('/edit-setting-record','ValidateController@EditSettingRecord')->name('EditSettingRecord');
Route::any('/ValidateFormData','ValidateController@ValidateFormData');
// Route::any('/ValidateFormData', function () {
//     return 'Enter';
// })->name('ValidateFormData');
Route::get('/form-data', function () {
    return view('index');
})->name('CheckFormData');
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
