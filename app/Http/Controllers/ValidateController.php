<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use  App\User;
use  App\Model\Setting;
use  App\Model\Reservation;
use Validator,DB,URL,Redirect,Session;
class ValidateController extends Controller
{
    public function ValidateFormData(Request $request)
    {



        //Fetch restriction setting record to check all restrictions
        $Record=Setting::first();
        //create a array to fill comman seprate userids
        $UserIDArray=explode(',',$request->UserIds);
        //check restriction condition according to restriction setting
        if(count($UserIDArray)>'1' && $Record->g=='individual') 
        {
            return response(['status'=>0,'message'=>'Group Not Allowed']);
        }
        else if(count($UserIDArray)<='1' && $Record->g=='group') 
        {
            return response(['status'=>0,'message'=>'Individual Not Allowed']);
        }
        else if(count($UserIDArray)<='1' && $Record->g=='individual')
        {
            //check individual entry on form submission if user id 1 and record submitted again on same userid with same day then we get already exits response
            $Exits=Reservation::where('user_id',$request->UserIds)->where('reservation_timestamp_utc',strtotime($request->Reservationdate))->get();
            if(count($Exits)>0){
                return response(['status'=>0,'message'=>'Record Already Exits']);
            }else{
                //insert record in reservation table
                $Insert=new Reservation;
                $Insert->user_id=$request->UserIds;
                $Insert->reservation_timestamp_utc=strtotime($request->Reservationdate);
                $Insert->save();
                    if($Insert->id)
                    {
                        return response(['status'=>1,'message'=>'Record Inserted Successfully!']); 
                    }
                }
        }
        else if(count($UserIDArray)>'1' && $Record->g=='group')
        {
            //insert group entry in reservation table
            foreach($UserIDArray as $value){
            $Insert=new Reservation;
            $Insert->user_id=$value;
            $Insert->reservation_timestamp_utc=strtotime($request->Reservationdate);
            $Insert->save();
           }
            if($Insert->id)
            {
                return response(['status'=>1,'message'=>'Record Inserted Successfully!']); 
            }
        }

    }
    //Edit restriction setting record
    public function EditSettingRecord(Request $request)
    {
        if ($request->isMethod('post')) {
           $Record=Setting::find($request->id);
           $Record->n=$request->n;
           $Record->d=$request->d;
           $Record->g=$request->g;
           $Record->tz=$request->tz;
           $Record->save();
           if($Record->id)
           {
            Session::flash('message', 'Record Updated Successfully!'); 
            return Redirect::back();
           }
        }else{
        $Record=Setting::first();
        return view('edit',compact('Record'));
        }
    }
	

}
